# bw's xonotic duel balance

I felt that duel in Xonotic lacked a bit of depth of decisionmaking because it plays just a little too fast. A large part of that speed is the blaster that allows you to jump to almost anywhere or instantly accelerate to max speed while only losing around 15 health. The blaster also makes skillfull movement techniques pretty useless by comparison. So I decided to make an experimental balance that just ouright removes it (along with other changes) to see how it would impact duels.

Furthermore I think that doing only 65% self-damage encourages unthinking pushes into the enemy with splash damage weapons if you have a large stack, so I increased it to 100%.

To compensate for the lack of blaster and 100% self-damage, I slightly increased the threshold for health and armor rot and made the rot non-linear so it slows down the closer you get to the threshold.

I also felt that the game as a whole has too much push force, so I decided to make some slight changes to devastator and vortex force to start with.

With blaster no longer being in the game I decided to make crylink a more specialized "movement manipulation" weapon. To make it more viable as a tool to stop your opponents I increased the travel speed of its secondary. To allow for it to be used on yourself more frequently I decreased the secondary's damage.

## Ideas 

These are some ideas I have for further changes. Discussion and feedback is welcome
* remove devastator detonation (2ndary): deva is already a very powerful weapon
* reduce push forces further - I'd prefer to first see how nerfing deva and vortex push impacts the game
* increase the lifetime of mortar secondary grenades to allow for better area denial

## Changelog

### 0.2

##### weapon balance

* reduced machinegun primary refire rate to 0.15 (xpm: 0.2)
* reduced electro secondary speed to 800 (xpm: 1000)
* decrease electro secondary refire rate, it's now somewhere around 0.3 (xpm: 0.2)
* increased crylink secondary speed to 6000 (xpm: 3000)
* decreased crylink secondary damage to 6 (xpm: 8)

### 0.1

##### general balance

* remove blaster, replace it with MG as starting weapon
* players now spawn with 120 MG ammo
* set self-damage to 100% (xpm: 65%)
* health rot now starts when over 125 health (xpm: 100)
* armor rot now starts when over 120 armor (xpm: 100)
* health and armor rot now pause for 3 seconds when picking up health/armor
* health and armor rot are now non-linear (like regeneration)

##### weapon balance

* set mortar refire rate to 0.7 (xpm: 0.8) to bring it in line with mortar secondary
* reduce devastator push force to 300 (xpm: 400)
* reduce vortex push force to 300 (xpm: 400)
* reduce devastator remote detonation (2ndary) force to 200 (xpm: 300)
* set machinegun push force to 0 (xpm: 3)
